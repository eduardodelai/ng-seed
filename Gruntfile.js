module.exports = function(grunt) {

	//time how long tasks take
	require('time-grunt')(grunt);

	//project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		less: {
			development: {
				options: {
					paths: [
						'assets/css/'
					]
				},
				files: [
					{
					    expand: true,
					    cwd: 'assets/css/',
					    src: ['*.less'],
					    dest: 'assets/css/',
					    ext: '.css'
					}
				]
			},
			production: {
				options: {
					paths: [
						'assets/css/'
					],
					compress: true
				},
				files: [
					{
					    expand: true,
					    cwd: 'assets/css/',
					    src: ['*.less'],
					    dest: 'assets/css/',
					    ext: '.min.css'
					}
				]
			}
		}
	});

	//loading plugins
	grunt.loadNpmTasks('grunt-contrib-less');	
	grunt.loadNpmTasks('grunt-newer');

	//default tasks
	grunt.registerTask('default', ['newer:less:development']);
	grunt.registerTask('production', ['newer:less:production']);

};