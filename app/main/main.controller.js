(function() {
    'use strict';

    angular
        .module('ngSeed')
        .controller('Main', Main);

    Main.$inject = [];

    /* @ngInject */
    function Main() {
        activate();

        /* Private Attributes Declaration */
        var self = this;
        /* ****************************** */

        /* Public Attributes Declaration */
        self.title = 'ng-seed';
        self.message = 'Hi, from the main page!';
        /* ***************************** */

        /* Private Methods Declaration */
        /* *************************** */

        /* Public Methods Declaration */
        /* ************************** */

        /* Activate */
        function activate() {
        }
        /* ******** */
    }
})();
