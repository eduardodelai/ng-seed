AngularJS Project Seed
Author: Eduardo Delai <edelaicorrea@gmail.com>
Version: 0.3.1

########## Intro ##########
This repository is intended to serve as a starting point for AngularJS projects.
With a few simple steps you it can jump start you right into writing valuable code and testing them.

####### Requirements ######
You will need the following:
  - NodeJS <nodejs.org/>
  - Bower <bower.io/>

######## Setting up #######
This steps will set up and running:
  - Download dependencies using bower: 'bower install'
  - Install NodeJS dependencies: 'npm install'
  - Start http server: 'npm start'

###### App Structure ######
When necessary add the following directories:
  - assets/css/ - Styling
  - assets/fonts/ - Fonts
  - assets/imgs/ - Images
  - app/components/ - First party components
  - app/layout/ - Common layout components, such as navigation and footer
  - app/* - Organize views or group of views in its own folder,  ie. app/main
  - tests/ - Specs for server integration and multiple components tests
Refer to johnpapa.net/angular-app-structuring-guidelines/

######### Tips ##########
  - It's strongly recommended to follow JohnPapa's angular-styleguide (github.com/johnpapa/angular-styleguide);
  - For custom Sublime Text 2 snippets, check bitbucket.org/eduardodelai/angularjs-sublime-snippets.